$(document).foundation();


// WOW JS enable functions that make the Animations render when the Users scroll towards them

$(function () {
  var wow = new WOW({
    boxClass: "wow",
    animateClass: "is-animating",
  }).init();
});


// Examples of JavaScript Effects and Transitions

$(function () {
  var $effect = $("#topbar");
  Foundation.Motion.animateIn($effect, "hinge-in");
});

$(function () {
  var $effect = $("#heroTitle");
  Foundation.Motion.animateIn($effect, "fade-in");
});

$(function () {
  var $effect = $("#orbit");
  Foundation.Motion.animateIn($effect, "fade-in");
});

$(function () {
  var $effect = $("#featuredArticles");
  Foundation.Motion.animateIn($effect, "fade-in");
});

$(function () {
  var $effect = $("#recentArticles");
  Foundation.Motion.animateIn($effect, "fade-in");
});

$(function () {
  var $effect = $("#newsArticles");
  Foundation.Motion.animateIn($effect, "fade-in");
});

$(function () {
  var $effect = $("#cultureArticles");
  Foundation.Motion.animateIn($effect, "fade-in");
});

$(function () {
  var $effect = $("#jobsArticles");
  Foundation.Motion.animateIn($effect, "fade-in");
});

$(function () {
  var $effect = $("#educationArticles");
  Foundation.Motion.animateIn($effect, "fade-in");
});